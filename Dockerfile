FROM ubuntu:18.04


ENV AWSCLI_LINK="https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
ENV AWSAUTH_LINK="https://s3.us-west-2.amazonaws.com/amazon-eks/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator"
ENV HELM_LINK="https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz"
ENV K8S_REL_TXT="https://storage.googleapis.com/kubernetes-release/release/stable.txt"
ENV K8S_REL_LNK="https://storage.googleapis.com/kubernetes-release/release/"



RUN apt-get update && \
apt-get install curl -y && \
apt-get install zip -y && \
apt-get install wget

# Install kubectl:

RUN curl -LO $K8S_REL_LNK`curl -s $K8S_REL_TXT`/bin/linux/amd64/kubectl && \
chmod +x ./kubectl && \
mv ./kubectl /usr/local/bin/kubectl


# Install AWS:

RUN curl $AWSCLI_LINK -o "awscliv2.zip" && \
unzip awscliv2.zip && \
./aws/install && \
rm -r aws && \
rm awscliv2.zip


# Install aws-iam-authenticator:

RUN curl -o aws-iam-authenticator $AWSAUTH_LINK && \
chmod +x ./aws-iam-authenticator && \
mkdir -p $HOME/bin && \
cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && \
export PATH=$PATH:$HOME/bin && \
echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc


# Install Helm:

RUN wget $HELM_LINK && \
tar -zxvf helm-v3.9.0-linux-amd64.tar.gz && \
mv linux-amd64/helm /usr/local/bin/helm && \
rm -r linux-amd64




